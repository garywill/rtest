<html>
    <head>
        <title></title>
        <meta HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=UTF-8"/>
        <META HTTP-EQUIV="Cache-Control" CONTENT="no-cache, must-revalidate">
        
        <script type="text/javascript" src="jquery.js?v=<?php echo filemtime("jquery.js");?>"></script>
        <style>
            body button { margin: 0px 5px; padding: 0px;font-size:11px; outline: none;border:0px;}
            td {  vertical-align: top;}
            body {text-align:center;}
            .red {color:red; }
        </style>
		<script type="text/javascript">
            function json_pretty(obj){
                return JSON.stringify(obj, null, 2);
            }
            function console_obj(obj){
                console.log( json_pretty( obj ) );
            }
		</script>
    </head>
    
    <body>
        <table>
            <tr>
                <td style="width:90%;">
                    <script type="text/javascript">
                        href_array = [
                            "index.php",
                            "same.php",
                            "http://localhost:9999/index.php",
                            "http://localhost:9999/same.php",
                            "http://127.0.0.1:7777/index.php",
                            "http://127.0.0.1:7777/same.php",
                        ];
                        href_array.forEach(function(item) 
                        {
                            document.write(`<div addr="${item}">`);
                            document.write(`<a href="${item}">${item}</a> `);
                            document.write("<br>");
                            document.write(
                                "<button onclick='location.replace(this.parentNode.attributes.addr.nodeValue);' class='btn-location-replace'>location.replace</button>"
                                + "<button onclick='location.href=this.parentNode.attributes.addr.nodeValue;' class='btn-location-href'>location.href</button>"
                                + "<button onclick='location.assign(this.parentNode.attributes.addr.nodeValue);' class='btn-location-assign'>location.assign</button>"
                                + "<button onclick='window.location.replace(this.parentNode.attributes.addr.nodeValue);' class='btn-window-location-replace'>window.location.replace</button>"
                                + "<button onclick='window.location.href=this.parentNode.attributes.addr.nodeValue;' class='btn-window-location-href'>window.location.href</button>"
                                + "<button onclick='window.location.assign(this.parentNode.attributes.addr.nodeValue);' class='btn-window-location-assign'>window.location.assign</button>"
                                + "<button onclick='document.location.replace(this.parentNode.attributes.addr.nodeValue);' class='btn-document-location-replace'>document.location.replace</button>"
                                + "<button onclick='document.location.href=this.parentNode.attributes.addr.nodeValue;' class='btn-document-location-href'>document.location.href</button>"
                                + "<button onclick='document.location.assign(this.parentNode.attributes.addr.nodeValue)' class='btn-document-location-assign'>document.location.assign</button>"
                                + "<button onclick='self.location.replace(this.parentNode.attributes.addr.nodeValue);' class='btn-self-location-replace'>self.location.replace</button>"
                                + "<button onclick='self.location.href=this.parentNode.attributes.addr.nodeValue;' class='btn-self-location-href'>self.location.href</button>"
                                + "<button onclick='self.location.assign(this.parentNode.attributes.addr.nodeValue);' class='btn-self-location-assign'>self.location.assign</button>"
                                + "<button onclick='top.location.replace(this.parentNode.attributes.addr.nodeValue);' class='btn-top-location-replace'>top.location.replace</button>"
                                + "<button onclick='top.location.href=this.parentNode.attributes.addr.nodeValue;' class='btn-top-location-href'>top.location.href</button>"
                                + "<button onclick='top.location.assign(this.parentNode.attributes.addr.nodeValue);' class='btn-top-location-assign'>top.location.assign</button>"
                                + "<button onclick='metarefresh(this)' class='btn-meta-refresh'>meta refresh</button>"
                                + "<button onclick='g301(this)' class='btn-301'>301</button>"
                                + "<button onclick='g302(this)' class='btn-302'>302</button>"
                                + "<button onclick='window.open(this.parentNode.attributes.addr.nodeValue);' class='btn-window-open'>window.open</button>"
                            );
                            document.write("</div>");
                        });
                        function metarefresh(obj)
                        {
                            var addr = obj.parentNode.attributes.addr.nodeValue;
                            var mida = document.createElement("a");
                            mida.rel = "noreferer";
                            mida.href = "metarefresh.php?url=" + addr;
                            mida.click();
                        }
                        function g301(obj)
                        {
                            var addr = obj.parentNode.attributes.addr.nodeValue;
                            var mida = document.createElement("a");
                            mida.rel = "noreferer";
                            mida.href = "301.php?url=" + addr;
                            mida.click();
                        }
                        function g302(obj)
                        {
                            var addr = obj.parentNode.attributes.addr.nodeValue;
                            var mida = document.createElement("a");
                            mida.rel = "noreferer";
                            mida.href = "302.php?url=" + addr;
                            mida.click();
                        }
                    </script>
                </td>
                <td>
                    Headers:<br>
                    <?php 
                        $headers =  getallheaders();
                        echo "Referer: <span class='red'>" . $headers["Referer"] . "</span><br>";
                        echo "X-Forwarded-Host: <span class='red'>" . $headers["X-Forwarded-Host"] . "</span><br>";
                        echo "Origin: <span class='red'>" . $headers["Origin"] . "</span><br>";
                    ?>
                    <br>
                    <textarea cols="10" rows="2"><?php 
                            foreach($headers as $key=>$val)
                            {
                                echo $key . ': ' . $val . "\n";
                            }   
                        ?>
                    </textarea>
                    
                    <br>JS:<br>
                    window.opener: 
                    <span class="red">
                    <script type="text/javascript">
                        if(window.opener){
                            document.write(window.opener);
                            document.write("<br>");
                            document.write("(location) "+window.opener.location.href);
                            document.write("<br>(referrer) "+window.opener.document.referrer);
                        }
                    </script>
                    </span>
                    <br>
                    
                    document.referrer: 
                    <span class="red">
                    <script type="text/javascript">
                        document.write(document.referrer);
                    </script>
                    </span>
                    
                    <br>
                    Object.getOwnPropertyDescriptor(document, "referrer"): 
                    <span class="red">
                    <script type="text/javascript">
                        document.write(
                            json_pretty( 
                                Object.getOwnPropertyDescriptor(document, "referrer") 
                                )
                        );
                    </script>
                    </span>
                    <br>
                    
                    Reflect.getOwnPropertyDescriptor(document, "referrer"): 
                    <span class="red">
                    <script type="text/javascript">
                        document.write(
                            json_pretty( 
                                Reflect.getOwnPropertyDescriptor(document, "referrer")
                                )
                        );
                    </script>
                    </span>
                    
                    <br>
                    Object.getOwnPropertyDescriptor(frameWindow.Document.prototype, "referrer").get.call(document): 
                    <span class="red">
                    <script type="text/javascript">
                        var div = document.createElement("div");
                        div.innerHTML = "<iframe style='height: 1px; width: 1px;'></iframe>"; // this line could also be below the appendChild
                        var framesLength = window.length;
                        document.body.appendChild(div);
                        var frameWindow = window[framesLength];
                        document.write(
                            json_pretty( 
                                Object.getOwnPropertyDescriptor(frameWindow.Document.prototype, "referrer").get.call(document) 
                                )
                        );
                    </script>
                    </span>
                    
                    <br>
                    Reflect.getOwnPropertyDescriptor(frameWindow.Document.prototype, "referrer").get.call(document): 
                    <span class="red">
                    <script type="text/javascript">
                        var div = document.createElement("div");
                        div.innerHTML = "<iframe style='height: 1px; width: 1px;'></iframe>"; // this line could also be below the appendChild
                        var framesLength = window.length;
                        document.body.appendChild(div);
                        var frameWindow = window[framesLength];
                        document.write(
                            json_pretty( 
                                Reflect.getOwnPropertyDescriptor(frameWindow.Document.prototype, "referrer").get.call(document) 
                                )
                        );
                    </script>
                    </span>
                    
                    <br>
                    frameWindow.Object.getOwnPropertyDescriptor.call(Object, document, "referrer"): 
                    <span class="red">
                    <script type="text/javascript">
                        var div = document.createElement("div");
                        div.innerHTML = "<iframe style='height: 1px; width: 1px;'></iframe>"; // this line could also be below the appendChild
                        var framesLength = window.length;
                        document.body.appendChild(div);
                        var frameWindow = window[framesLength];
                        document.write(
                            json_pretty( 
                                frameWindow.Object.getOwnPropertyDescriptor.call(Object, document, "referrer")
                                )
                        );
                    </script>
                    </span>
                    
                    <br>
                    frameWindow.Object.getOwnPropertyDescriptor.call(Reflect, document, "referrer"): 
                    <span class="red">
                    <script type="text/javascript">
                        var div = document.createElement("div");
                        div.innerHTML = "<iframe style='height: 1px; width: 1px;'></iframe>"; // this line could also be below the appendChild
                        var framesLength = window.length;
                        document.body.appendChild(div);
                        var frameWindow = window[framesLength];
                        document.write(
                            json_pretty( 
                                frameWindow.Object.getOwnPropertyDescriptor.call(Reflect, document, "referrer")
                                )
                        );
                    </script>
                    </span>
                    
                    <br>
                    frameWindow.Reflect.getOwnPropertyDescriptor.call(Reflect, document, "referrer"): 
                    <span class="red">
                    <script type="text/javascript">
                        var div = document.createElement("div");
                        div.innerHTML = "<iframe style='height: 1px; width: 1px;'></iframe>"; // this line could also be below the appendChild
                        var framesLength = window.length;
                        document.body.appendChild(div);
                        var frameWindow = window[framesLength];
                        document.write(
                            json_pretty( 
                                frameWindow.Reflect.getOwnPropertyDescriptor.call(Reflect, document, "referrer")
                                )
                        );
                    </script>
                    </span>
                    
                    <br>
                    frameWindow.Reflect.getOwnPropertyDescriptor.call(Object, document, "referrer"): 
                    <span class="red">
                    <script type="text/javascript">
                        var div = document.createElement("div");
                        div.innerHTML = "<iframe style='height: 1px; width: 1px;'></iframe>"; // this line could also be below the appendChild
                        var framesLength = window.length;
                        document.body.appendChild(div);
                        var frameWindow = window[framesLength];
                        document.write(
                            json_pretty( 
                                frameWindow.Reflect.getOwnPropertyDescriptor.call(Object, document, "referrer")
                                )
                        );
                    </script>
                    </span>
                </td>
            </tr>
        </table>
        <iframe style="display:inline-block; width:90%; height:600px; " src="index.php">
        
        </iframe>

    </body>



</html>
